package au.com.reece.branch.addressbook.util;

import au.com.reece.branch.addressbook.entities.ContactEntity;
import au.com.reece.branch.addressbook.entities.ContactNumberEntity;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class CommonUtilsTest {

    /**
     * Test retrieving unique contacts based on contact name
     *
     * TODO: Need to enhance comparing phone numbers as well
     */
    @Test
    public void testUniqueContacts() {

        List<ContactEntity> contacts = new ArrayList<>();

        contacts.add(ContactEntity.of("A Duplicate Contact 1",
            Arrays.asList(
                ContactNumberEntity.of(ContactNumberEntity.ContactNumberType.Mobile, "+61 5631 5219", null),
                ContactNumberEntity.of(ContactNumberEntity.ContactNumberType.Work, "+61 2335 1856", "246"),
                ContactNumberEntity.of(ContactNumberEntity.ContactNumberType.Company, "+61 3960 3883", "235"))));

        contacts.add(ContactEntity.of("A Duplicate Contact 1",
            Arrays.asList(
                ContactNumberEntity.of(ContactNumberEntity.ContactNumberType.Mobile, "+61 5631 5219", null),
                ContactNumberEntity.of(ContactNumberEntity.ContactNumberType.Work, "+61 2335 1856", "246"),
                ContactNumberEntity.of(ContactNumberEntity.ContactNumberType.Company, "+61 3960 3883", "235"))));

        List<ContactEntity> distinctContacts = CommonUtils.retrieveDistinctContacts(contacts);

        assertThat(distinctContacts.size(), is(1));
    }
}
