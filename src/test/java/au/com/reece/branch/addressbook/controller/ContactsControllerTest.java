package au.com.reece.branch.addressbook.controller;

import au.com.reece.branch.addressbook.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import static au.com.reece.branch.addressbook.util.TestConstants.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class ContactsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private static ObjectMapper objectMapper;

    @BeforeAll
    static void initAll() {

        // Initialize Jackson mapper to convert response json to object
        objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    @Test
    void testContactCreation() {
        try {
            AddressBook addressBook = createAddressBook();
            Contact contact = prepareContact();

            MvcResult result = createContact(addressBook, contact);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(CREATED.value()));

            Created createdResponse = objectMapper.readValue(result.getResponse().getContentAsString(), Created.class);
            assertThat(createdResponse, is(notNullValue()));
            assertThat(createdResponse.getId(), is(notNullValue()));

            assertThat(createdResponse.getId(), is(greaterThan(0l)));
        }
        catch (Exception e) {
            fail("Error occurred while testing contact creation", e);
        }
    }

    @Test
    void testContactRetrieval() {
        try {
            AddressBook addressBook = createAddressBook();
            Contact contact = prepareContact();

            MvcResult result = createContact(addressBook, contact);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(CREATED.value()));

            Created createdResponse = objectMapper.readValue(result.getResponse().getContentAsString(), Created.class);

            result = getContacts(addressBook, 1,1000);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(OK.value()));

            AddressBookContacts addressBookContacts = objectMapper.readValue(result.getResponse().getContentAsString(), AddressBookContacts.class);
            assertThat(addressBookContacts, is(notNullValue()));
            assertThat(addressBookContacts.getData(), is(notNullValue()));
            assertThat(addressBookContacts.getPaging(), is(notNullValue()));
            assertThat(addressBookContacts.getData(), is(not(empty())));

            Optional<Contact> createdContact = addressBookContacts.getData().stream().filter(ab -> ab.getId() == createdResponse.getId()).findAny();
            assertTrue(createdContact.isPresent());

            assertThat(createdContact.get().getName(), is(contact.getName()));
            assertThat(createdContact.get().getNotes(), is(contact.getNotes()));

            createdContact.get().getContactNumbers().stream().forEach(createdContactNumber -> {

                assertThat(contact.getContactNumbers(), hasItem(Matchers.<ContactNumber>hasProperty("number", equalTo(createdContactNumber.getNumber()))));
                assertThat(contact.getContactNumbers(), hasItem(Matchers.<ContactNumber>hasProperty("type", equalTo(createdContactNumber.getType()))));
                assertThat(contact.getContactNumbers(), hasItem(Matchers.<ContactNumber>hasProperty("extension", equalTo(createdContactNumber.getExtension()))));
            });
        }
        catch (Exception e) {
            fail("Error occurred while testing retrieving creation", e);
        }
    }

    @Test
    void testContactRetrievalByName() {
        try {
            AddressBook addressBook = createAddressBook();
            Contact contact = prepareContact();

            MvcResult result = createContact(addressBook, contact);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(CREATED.value()));

            Created createdResponse = objectMapper.readValue(result.getResponse().getContentAsString(), Created.class);

            result
                = this.mockMvc
                    .perform(
                        get(BASE_PATH + CONTACT_GET_CREATE, GLOBAL_BRANCH_ID, addressBook.getId())
                            .header(HEADER_CORRELATION_ID, uuid())
                            .param(QUERY_PARAM_PAGE, String.valueOf(1))
                            .param(QUERY_PARAM_PAGELIMIT, String.valueOf(1000))
                            .param(QUERY_PARAM_CONTACTNAME, contact.getName())
                            .contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andReturn();
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(OK.value()));

            AddressBookContacts addressBookContacts = objectMapper.readValue(result.getResponse().getContentAsString(), AddressBookContacts.class);
            assertThat(addressBookContacts, is(notNullValue()));
            assertThat(addressBookContacts.getData(), is(notNullValue()));
            assertThat(addressBookContacts.getPaging(), is(notNullValue()));
            assertThat(addressBookContacts.getData(), is(not(empty())));

            Optional<Contact> createdContact = addressBookContacts.getData().stream().filter(ab -> ab.getId() == createdResponse.getId()).findAny();
            assertTrue(createdContact.isPresent());

            assertThat(createdContact.get().getName(), is(contact.getName()));
            assertThat(createdContact.get().getNotes(), is(contact.getNotes()));

            createdContact.get().getContactNumbers().stream().forEach(createdContactNumber -> {

                assertThat(contact.getContactNumbers(), hasItem(Matchers.<ContactNumber>hasProperty("number", equalTo(createdContactNumber.getNumber()))));
                assertThat(contact.getContactNumbers(), hasItem(Matchers.<ContactNumber>hasProperty("type", equalTo(createdContactNumber.getType()))));
                assertThat(contact.getContactNumbers(), hasItem(Matchers.<ContactNumber>hasProperty("extension", equalTo(createdContactNumber.getExtension()))));
            });
        }
        catch (Exception e) {
            fail("Error occurred while testing retrieving creation", e);
        }
    }

    @Test
    void testContactUpdate() {
        try {
            AddressBook addressBook = createAddressBook();
            Contact contact = prepareContact();

            MvcResult result = createContact(addressBook, contact);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(CREATED.value()));

            Created createdResponse = objectMapper.readValue(result.getResponse().getContentAsString(), Created.class);

            // Update contact details
            contact.setName(random(25));
            contact.setNotes(random(1000));

            List<ContactNumber> contactNumbers = new ArrayList<>();
            List<ContactNumber.TypeEnum> types = Arrays.asList(ContactNumber.TypeEnum.values());
            IntStream.range(0, 3).forEach(i -> {
                ContactNumber contactNumber = new ContactNumber();
                contactNumber.setType(types.get(i));
                contactNumber.setNumber(randomInt(10));
                contactNumber.setExtension(randomInt(3));

                contactNumbers.add(contactNumber);
            });
            contact.setContactNumbers(contactNumbers);

            this.mockMvc
                .perform(
                    put(BASE_PATH + CONTACT_UPDATE_DELETE, GLOBAL_BRANCH_ID, addressBook.getId(), createdResponse.getId())
                        .header(HEADER_CORRELATION_ID, uuid())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(contact)))
                .andExpect(status().is(NO_CONTENT.value()))
                .andReturn();

            result = getContacts(addressBook, 1,1000);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(OK.value()));

            AddressBookContacts addressBookContacts = objectMapper.readValue(result.getResponse().getContentAsString(), AddressBookContacts.class);
            assertThat(addressBookContacts, is(notNullValue()));
            assertThat(addressBookContacts.getData(), is(notNullValue()));
            assertThat(addressBookContacts.getPaging(), is(notNullValue()));
            assertThat(addressBookContacts.getData(), is(not(empty())));

            Optional<Contact> updatedContact = addressBookContacts.getData().stream().filter(ab -> ab.getId() == createdResponse.getId()).findAny();
            assertTrue(updatedContact.isPresent());

            assertThat(updatedContact.get().getName(), is(contact.getName()));
            assertThat(updatedContact.get().getNotes(), is(contact.getNotes()));

            updatedContact.get().getContactNumbers().stream().forEach(createdContactNumber -> {

                assertThat(contact.getContactNumbers(), hasItem(Matchers.<ContactNumber>hasProperty("number", equalTo(createdContactNumber.getNumber()))));
                assertThat(contact.getContactNumbers(), hasItem(Matchers.<ContactNumber>hasProperty("type", equalTo(createdContactNumber.getType()))));
                assertThat(contact.getContactNumbers(), hasItem(Matchers.<ContactNumber>hasProperty("extension", equalTo(createdContactNumber.getExtension()))));
            });

        }
        catch (Exception e) {
            fail("Error occurred while testing updating a contact", e);
        }
    }

    @Test
    void testContactDeletion() {
        try {
            AddressBook addressBook = createAddressBook();
            Contact contact = prepareContact();

            MvcResult result = createContact(addressBook, contact);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(CREATED.value()));

            Created createdResponse = objectMapper.readValue(result.getResponse().getContentAsString(), Created.class);

            this.mockMvc
                .perform(
                    delete(BASE_PATH + CONTACT_UPDATE_DELETE, GLOBAL_BRANCH_ID, addressBook.getId(), createdResponse.getId())
                        .header(HEADER_CORRELATION_ID, uuid())
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(NO_CONTENT.value()))
                .andReturn();

            result = getContacts(addressBook, 1,1000);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(OK.value()));

            AddressBookContacts addressBookContacts = objectMapper.readValue(result.getResponse().getContentAsString(), AddressBookContacts.class);
            assertThat(addressBookContacts, is(notNullValue()));
            assertThat(addressBookContacts.getData(), is(notNullValue()));
            assertThat(addressBookContacts.getPaging(), is(notNullValue()));
            assertThat(addressBookContacts.getData(), is((empty())));
        }
        catch (Exception e) {
            fail("Error occurred while testing contact deletion", e);
        }
    }

    @Test
    void testContactPaginationDataExists() {
        try {
            AddressBook addressBook = createAddressBook();
            IntStream.of(10).forEach(i -> {
                try {
                    createContact(addressBook, prepareContact());
                }
                catch (Exception e) {
                    fail("Error occurred while creating test contacts for pagination test", e);
                }
            });

            int pageSize = 2;

            MvcResult result = getContacts(addressBook, 1,pageSize);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(OK.value()));

            AddressBookContacts addressBookContacts = objectMapper.readValue(result.getResponse().getContentAsString(), AddressBookContacts.class);
            assertThat(addressBookContacts, is(notNullValue()));
            assertThat(addressBookContacts.getData(), is(notNullValue()));
            assertThat(addressBookContacts.getPaging(), is(notNullValue()));
            assertThat(addressBookContacts.getData(), is(not(empty())));

            result = getContacts(addressBook, addressBookContacts.getPaging().getTotalPages(),pageSize);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(OK.value()));

            addressBookContacts = objectMapper.readValue(result.getResponse().getContentAsString(), AddressBookContacts.class);
            assertThat(addressBookContacts, is(notNullValue()));
            assertThat(addressBookContacts.getData(), is(notNullValue()));
            assertThat(addressBookContacts.getPaging(), is(notNullValue()));
            assertThat(addressBookContacts.getData(), is(not(empty())));
        }
        catch (Exception e) {
            fail("Error occurred while testing contacts pagination for data available for page that does exist");
        }
    }

    @Test
    void testContactPaginationNoDataAvailable() {
        try {
            AddressBook addressBook = createAddressBook();
            Contact contact = prepareContact();

            MvcResult result = createContact(addressBook, contact);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(CREATED.value()));

            result = getContacts(addressBook, 1,25);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(OK.value()));

            AddressBookContacts addressBookContacts = objectMapper.readValue(result.getResponse().getContentAsString(), AddressBookContacts.class);
            assertThat(addressBookContacts, is(notNullValue()));
            assertThat(addressBookContacts.getData(), is(notNullValue()));
            assertThat(addressBookContacts.getPaging(), is(notNullValue()));
            assertThat(addressBookContacts.getData(), is(not(empty())));

            int notAValidPage = addressBookContacts.getPaging().getTotalPages() + 100;
            result = getContacts(addressBook, notAValidPage,25);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(OK.value()));

            addressBookContacts = objectMapper.readValue(result.getResponse().getContentAsString(), AddressBookContacts.class);
            assertThat(addressBookContacts, is(notNullValue()));
            assertThat(addressBookContacts.getData(), is(notNullValue()));
            assertThat(addressBookContacts.getPaging(), is(notNullValue()));
            assertThat(addressBookContacts.getData(), is((empty())));
        }
        catch (Exception e) {
            fail("Error occurred while testing contacts pagination for data not available for page that does not exist");
        }
    }

    /**
     * Method to create address book upon startup for contact creation test
     * @return addressBook
     * @throws Exception
     */
    private AddressBook createAddressBook() throws Exception {

        AddressBook addressBook = new AddressBook();
        addressBook.setName(random(25));
        addressBook.setDescription(random(1000));
        addressBook.setTags(Arrays.asList(random(25), random(25)));

        MvcResult result
            = this.mockMvc
                .perform(
                    post(BASE_PATH + ADDRESSBOOK_GET_CREATE, GLOBAL_BRANCH_ID)
                        .header(HEADER_CORRELATION_ID, uuid())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(addressBook))
                )
                .andExpect(status().is(CREATED.value()))
                .andReturn();
        assertThat(result, is(notNullValue()));

        Created createdResponse = objectMapper.readValue(result.getResponse().getContentAsString(), Created.class);
        assertThat(createdResponse, is(notNullValue()));
        assertThat(createdResponse.getId(), is(notNullValue()));

        assertThat(createdResponse.getId(), is(greaterThan(0l)));

        addressBook.setId(createdResponse.getId());

        return addressBook;
    }

    private MvcResult createContact(AddressBook addressBook, Contact contact) throws Exception {
        MvcResult result
            = this.mockMvc
            .perform(
                post(BASE_PATH + CONTACT_GET_CREATE, GLOBAL_BRANCH_ID, addressBook.getId())
                    .header(HEADER_CORRELATION_ID, uuid())
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(objectMapper.writeValueAsString(contact))
            )
            .andReturn();
        return result;
    }

    private MvcResult getContacts(AddressBook addressBook, int page, int pageSize) throws Exception {
        MvcResult result
            = this.mockMvc
                .perform(
                    get(BASE_PATH + CONTACT_GET_CREATE, GLOBAL_BRANCH_ID, addressBook.getId())
                        .header(HEADER_CORRELATION_ID, uuid())
                        .param(QUERY_PARAM_PAGE, String.valueOf(page))
                        .param(QUERY_PARAM_PAGELIMIT, String.valueOf(pageSize))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        return result;
    }

    private Contact prepareContact() {
        Contact contact = new Contact();
        contact.setName(random(25));
        contact.setNotes(random(1000));

        List<ContactNumber> contactNumbers = new ArrayList<>();
        List<ContactNumber.TypeEnum> types = Arrays.asList(ContactNumber.TypeEnum.values());
        IntStream.range(0, 3).forEach(i -> {
            ContactNumber contactNumber = new ContactNumber();
            contactNumber.setType(types.get(i));
            contactNumber.setNumber(randomInt(10));
            contactNumber.setExtension(randomInt(3));

            contactNumbers.add(contactNumber);
        });
        contact.setContactNumbers(contactNumbers);

        return contact;
    }
}
