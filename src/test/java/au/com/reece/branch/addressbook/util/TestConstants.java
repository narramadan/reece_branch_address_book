package au.com.reece.branch.addressbook.util;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.UUID;

public class TestConstants {

    public static final Long GLOBAL_BRANCH_ID = 1l;
    public static final Long UNITTEST_BRANCH_ID = 3l;

    // ---------- Paths ------------------
    public static final String BASE_PATH = "/v1";
    public static final String ADDRESSBOOK_GET_CREATE = "/branches/{branchId}/addressbooks";
    public static final String ADDRESSBOOK_UPDATE_DELETE = "/branches/{branchId}/addressbooks/{addressBookId}";

    public static final String CONTACT_GET_CREATE = "/branches/{branchId}/addressbooks/{addressBookId}/contacts";
    public static final String CONTACT_UPDATE_DELETE = "/branches/{branchId}/addressbooks/{addressBookId}/contacts/{contactId}";

    public static final String PRINT_CONTACTS_UNDER_BRANCH = "/branches/{branchId}/addressbooks/print";
    public static final String PRINT_CONTACTS_UNDER_ADDRESSBOOK = "/branches/{branchId}/addressbooks/{addressBookId}/print";

    public static final String HEADER_CORRELATION_ID = "x-correlationId";
    public static final String QUERY_PARAM_PAGE = "page";
    public static final String QUERY_PARAM_PAGELIMIT = "limit";
    public static final String QUERY_PARAM_CONTACTNAME = "contactName";

    public static String uuid() {
        return UUID.randomUUID().toString();
    }

    public static String random(int length) {
        return RandomStringUtils.randomAlphanumeric(length);
    }

    public static String randomInt(int length) { return RandomStringUtils.randomNumeric(length); }

}
