package au.com.reece.branch.addressbook.controller;

import au.com.reece.branch.addressbook.model.AddressBook;
import au.com.reece.branch.addressbook.model.Contact;
import au.com.reece.branch.addressbook.model.ContactNumber;
import au.com.reece.branch.addressbook.model.Created;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.itextpdf.text.pdf.PdfReader;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static au.com.reece.branch.addressbook.util.TestConstants.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class PrintContactControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private static ObjectMapper objectMapper;

    @BeforeAll
    static void initAll() {

        // Initialize Jackson mapper to convert response json to object
        objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    @Test
    void testPrintAddressBooksContactsTest() {
        try {
            AddressBook addressBook = createAddressBook(GLOBAL_BRANCH_ID);

            // Load 20 contacts so there would be more than one page when they are exported to pdf
            IntStream.range(0, 20).forEach(i -> {
                try {
                    createContact(GLOBAL_BRANCH_ID, addressBook, prepareContact());
                }
                catch (Exception e) {
                    fail("Error occurred while creating test contacts for pagination test", e);
                }
            });

            MvcResult result
                = this.mockMvc
                    .perform(
                        get(BASE_PATH + PRINT_CONTACTS_UNDER_ADDRESSBOOK, GLOBAL_BRANCH_ID, addressBook.getId())
                            .header(HEADER_CORRELATION_ID, uuid())
                            .param(QUERY_PARAM_PAGE, String.valueOf(1))
                            .param(QUERY_PARAM_PAGELIMIT, String.valueOf(1000))
                            .contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().is(OK.value()))
                    .andReturn();
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getContentAsByteArray(), is(notNullValue()));

            Path path = Files.createTempFile(uuid(), ".pdf");
            File file = path.toFile();

            try (FileOutputStream outputStream = new FileOutputStream(file)) {
                outputStream.write(result.getResponse().getContentAsByteArray());
            }

            PdfReader pdfReader = new PdfReader(new FileInputStream(file));

            // Check if number of pages is greater than 1
            assertThat(pdfReader.getNumberOfPages(), is(greaterThan(1)));

        }
        catch (Exception e) {
            fail("Error occurred while generating pdf for address book contacts");
        }
    }

    @Test
    void testPrintBranchUniqueContactsAcrossAddressBook() {
        try {

            Contact contact = prepareContact();

            // Load 20 addressbooks with same contact details. Such that there is only one unique contact across all these address books and pdf contains only one page
            IntStream.range(0, 20).forEach(i -> {
                try {
                    AddressBook addressBook = createAddressBook(UNITTEST_BRANCH_ID);
                    createContact(UNITTEST_BRANCH_ID, addressBook, contact);
                }
                catch (Exception e) {
                    fail("Error occurred while creating test contacts for unique pagination test", e);
                }
            });

            MvcResult result
                = this.mockMvc
                    .perform(
                        get(BASE_PATH + PRINT_CONTACTS_UNDER_BRANCH, UNITTEST_BRANCH_ID)
                            .header(HEADER_CORRELATION_ID, uuid())
                            .param(QUERY_PARAM_PAGE, String.valueOf(1))
                            .param(QUERY_PARAM_PAGELIMIT, String.valueOf(1000))
                            .contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().is(OK.value()))
                    .andReturn();
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getContentAsByteArray(), is(notNullValue()));

            Path path = Files.createTempFile(uuid(), ".pdf");
            File file = path.toFile();

            try (FileOutputStream outputStream = new FileOutputStream(file)) {
                outputStream.write(result.getResponse().getContentAsByteArray());
            }

            PdfReader pdfReader = new PdfReader(new FileInputStream(file));

            // Check if number of pages is greater than 1
            assertThat(pdfReader.getNumberOfPages(), is(1));

        }
        catch (Exception e) {
            fail("Error occurred while generating pdf for branch unique contacts");
        }
    }

    /**
     * Method to create address book upon startup for contact creation test
     * @return addressBook
     * @throws Exception
     */
    private AddressBook createAddressBook(Long branchId) throws Exception {

        AddressBook addressBook = new AddressBook();
        addressBook.setName(random(25));
        addressBook.setDescription(random(1000));
        addressBook.setTags(Arrays.asList(random(25), random(25)));

        MvcResult result
            = this.mockMvc
                .perform(
                    post(BASE_PATH + ADDRESSBOOK_GET_CREATE, branchId)
                        .header(HEADER_CORRELATION_ID, uuid())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(addressBook))
                )
                .andExpect(status().is(CREATED.value()))
                .andReturn();
        assertThat(result, is(notNullValue()));

        Created createdResponse = objectMapper.readValue(result.getResponse().getContentAsString(), Created.class);
        assertThat(createdResponse, is(notNullValue()));
        assertThat(createdResponse.getId(), is(notNullValue()));

        assertThat(createdResponse.getId(), is(greaterThan(0l)));

        addressBook.setId(createdResponse.getId());

        return addressBook;
    }

    private MvcResult createContact(Long branchId, AddressBook addressBook, Contact contact) throws Exception {
        MvcResult result
            = this.mockMvc
                .perform(
                    post(BASE_PATH + CONTACT_GET_CREATE, branchId, addressBook.getId())
                        .header(HEADER_CORRELATION_ID, uuid())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(contact))
                )
                .andReturn();
        return result;
    }

    private Contact prepareContact() {
        Contact contact = new Contact();
        contact.setName(random(25));
        contact.setNotes(random(1000));

        List<ContactNumber> contactNumbers = new ArrayList<>();
        List<ContactNumber.TypeEnum> types = Arrays.asList(ContactNumber.TypeEnum.values());
        IntStream.range(0, 3).forEach(i -> {
            ContactNumber contactNumber = new ContactNumber();
            contactNumber.setType(types.get(i));
            contactNumber.setNumber(randomInt(10));
            contactNumber.setExtension(randomInt(3));

            contactNumbers.add(contactNumber);
        });
        contact.setContactNumbers(contactNumbers);

        return contact;
    }

}
