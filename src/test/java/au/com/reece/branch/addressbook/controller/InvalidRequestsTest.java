package au.com.reece.branch.addressbook.controller;

import static au.com.reece.branch.addressbook.util.TestConstants.*;
import au.com.reece.branch.addressbook.dao.BranchRepository;
import au.com.reece.branch.addressbook.entities.BranchEntity;
import au.com.reece.branch.addressbook.exception.ErrorCodes;
import au.com.reece.branch.addressbook.model.AddressBook;
import au.com.reece.branch.addressbook.model.ErrorResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.springframework.http.HttpStatus.*;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.validation.ConstraintViolationException;
import java.util.Optional;

import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
class InvalidRequestsTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BranchRepository branchRepository;

    private static ObjectMapper objectMapper;

    @BeforeAll
    static void initAll() {

        // Initialize Jackson mapper to convert response json to object
        objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    @Test
    void testResourceNotFound() {
        try {
            given(branchRepository.findById(anyLong())).willReturn(Optional.empty());

            MvcResult result
                = this.mockMvc
                    .perform(
                        get(BASE_PATH + ADDRESSBOOK_GET_CREATE, 1)
                            .header(HEADER_CORRELATION_ID, uuid())
                            .contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().is(NOT_FOUND.value()))
                    .andReturn();

            assertEquals(
                objectMapper.readValue(result.getResponse().getContentAsString(), ErrorResponse.class)
                    .getErrors().get(0).getCode(),
                ErrorCodes.ERR_RESOURCE_NOT_FOUND
            );
        }
        catch(Exception e) {
            fail("Error occurred while testing resource not found", e);
        }
    }

    @Test
    void testInvalidRequestParam() {
        try {
            given(branchRepository.findById(anyLong())).willReturn(Optional.ofNullable(new BranchEntity()));

            MvcResult result
                = this.mockMvc
                    .perform(
                        get(BASE_PATH + ADDRESSBOOK_GET_CREATE, "INVALID")
                            .header(HEADER_CORRELATION_ID, uuid())
                            .contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().is(BAD_REQUEST.value()))
                    .andReturn();

            assertEquals(
                objectMapper.readValue(result.getResponse().getContentAsString(), ErrorResponse.class)
                    .getErrors().get(0).getCode(),
                ErrorCodes.ERR_REQUEST_PARAM_INVALID
            );
        }
        catch(Exception e) {
            fail("Error occurred while testing invalid param", e);
        }
    }

    @Test
    void testInvalidRequestBody() {
        try {
            MvcResult result
                = this.mockMvc
                    .perform(
                        post(BASE_PATH + ADDRESSBOOK_GET_CREATE, 1)
                            .header(HEADER_CORRELATION_ID, uuid())
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(objectMapper.writeValueAsString(new AddressBook()))
                    )
                    .andExpect(status().is(BAD_REQUEST.value()))
                    .andReturn();

            assertEquals(
                objectMapper.readValue(result.getResponse().getContentAsString(), ErrorResponse.class)
                    .getErrors().get(0).getCode(),
                ErrorCodes.ERR_REQUEST_BODY_INVALID
            );
        }
        catch(Exception e) {
            fail("Error occurred while testing invalid request body", e);
        }
    }

    @Test
    void testInvalidPageQueryParam() {
        try {
            given(branchRepository.findById(anyLong())).willReturn(Optional.ofNullable(new BranchEntity()));

            MvcResult result
                = this.mockMvc
                    .perform(
                        get(BASE_PATH + ADDRESSBOOK_GET_CREATE, "1")
                            .header(HEADER_CORRELATION_ID, uuid())
                            .param("page", "INVALID")
                            .contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().is(BAD_REQUEST.value()))
                    .andReturn();

            assertEquals(
                objectMapper.readValue(result.getResponse().getContentAsString(), ErrorResponse.class)
                    .getErrors().get(0).getCode(),
                ErrorCodes.ERR_REQUEST_PARAM_INVALID
            );
        }
        catch(Exception e) {
            fail("Error occurred while testing invalid page param", e);
        }
    }

    @Test
    void testMandatoryHeaderMissing() {
        try {
            given(branchRepository.findById(anyLong())).willReturn(Optional.ofNullable(new BranchEntity()));

            MvcResult result
                = this.mockMvc
                    .perform(
                        get(BASE_PATH + ADDRESSBOOK_GET_CREATE, 1)
                            .contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().is(BAD_REQUEST.value()))
                    .andReturn();

            assertEquals(
                objectMapper.readValue(result.getResponse().getContentAsString(), ErrorResponse.class)
                    .getErrors().get(0).getCode(),
                ErrorCodes.ERR_HEADER_MISSING
            );
        }
        catch(Exception e) {
            fail("Error occurred while testing invalid header", e);
        }
    }

    @Test
    void testRuntimeException() {
        try {
            given(branchRepository.findById(anyLong())).willThrow(ConstraintViolationException.class);

            this.mockMvc
                .perform(
                    get(BASE_PATH + ADDRESSBOOK_GET_CREATE, GLOBAL_BRANCH_ID)
                        .header(HEADER_CORRELATION_ID, uuid())
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().is(INTERNAL_SERVER_ERROR.value()))
                .andReturn();
        }
        catch(Exception e) {
            Assertions.fail("Error occurred while verifying runtime exception", e);
        }

    }
}