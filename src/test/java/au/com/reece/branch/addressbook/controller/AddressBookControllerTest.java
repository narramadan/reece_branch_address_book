package au.com.reece.branch.addressbook.controller;

import au.com.reece.branch.addressbook.exception.ErrorCodes;
import au.com.reece.branch.addressbook.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.Optional;

import static au.com.reece.branch.addressbook.util.TestConstants.*;
import static au.com.reece.branch.addressbook.util.TestConstants.uuid;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class AddressBookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private static ObjectMapper objectMapper;

    @BeforeAll
    static void initAll() {

        // Initialize Jackson mapper to convert response json to object
        objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    /**
     * Validate invalid request for address book creation.
     */
    @Test
    void testAddressBookCreationInvalidRequestFields() {
        try {
            AddressBook addressBook = new AddressBook();
            addressBook.setName(random(100));
            addressBook.setDescription(random(2000));
            addressBook.setTags(Arrays.asList(random(50), random(50)));

            MvcResult result = createAddressBook(addressBook);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(BAD_REQUEST.value()));

            ErrorResponse errorResponse = objectMapper.readValue(result.getResponse().getContentAsString(), ErrorResponse.class);
            assertThat(errorResponse, is(notNullValue()));
            assertThat(errorResponse.getErrors(), is(not(empty())));
        }
        catch (Exception e) {
            fail("Error occurred while testing address book creation with invalid request", e);
        }
    }

    @Test
    void testAddressBookCreation() {
        try {
            AddressBook addressBook = prepareAddressBook();

            MvcResult result = createAddressBook(addressBook);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(CREATED.value()));

            Created createdResponse = objectMapper.readValue(result.getResponse().getContentAsString(), Created.class);
            assertThat(createdResponse, is(notNullValue()));
            assertThat(createdResponse.getId(), is(notNullValue()));

            assertThat(createdResponse.getId(), is(greaterThan(0l)));
        }
        catch (Exception e) {
            fail("Error occurred while testing address book creation", e);
      }
    }

    /**
     * Test retrieval of created address book and compare created and returned address book fields
     */
    @Test
    void testAddressBookRetrieval() {
        try {
            AddressBook addressBook = prepareAddressBook();

            MvcResult result = createAddressBook(addressBook);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(CREATED.value()));

            Created createdResponse = objectMapper.readValue(result.getResponse().getContentAsString(), Created.class);

            result = getAddressBooks(1,1000);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(OK.value()));

            AddressBooksResponse addressBooksResponse = objectMapper.readValue(result.getResponse().getContentAsString(), AddressBooksResponse.class);
            assertThat(addressBooksResponse, is(notNullValue()));
            assertThat(addressBooksResponse.getData(), is(notNullValue()));
            assertThat(addressBooksResponse.getPaging(), is(notNullValue()));
            assertThat(addressBooksResponse.getData(), is(not(empty())));

            Optional<AddressBook> createdAddressBook = addressBooksResponse.getData().stream().filter(ab -> ab.getId() == createdResponse.getId()).findAny();
            assertTrue(createdAddressBook.isPresent());

            assertThat(createdAddressBook.get().getName(), is(addressBook.getName()));
            assertThat(createdAddressBook.get().getDescription(), is(addressBook.getDescription()));
            assertThat(createdAddressBook.get().getTags(), containsInAnyOrder(addressBook.getTags().toArray()));
        }
        catch (Exception e) {
            fail("Error occurred while testing address book retrieval", e);
        }
    }

    /**
     * Test updating address book and validate if details are updated as expected by validating with retrieved address book
     */
    @Test
    void testAddressBookUpdate() {
        try {
            AddressBook addressBook = prepareAddressBook();

            MvcResult result = createAddressBook(addressBook);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(CREATED.value()));

            Created createdResponse = objectMapper.readValue(result.getResponse().getContentAsString(), Created.class);

            addressBook.setName(random(25));
            addressBook.setDescription(random(1000));
            addressBook.setTags(Arrays.asList(random(25), random(25)));
            this.mockMvc
                .perform(
                    put(BASE_PATH + ADDRESSBOOK_UPDATE_DELETE, GLOBAL_BRANCH_ID, createdResponse.getId())
                        .header(HEADER_CORRELATION_ID, uuid())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(addressBook)))
                .andExpect(status().is(NO_CONTENT.value()))
                .andReturn();

            result = getAddressBooks(1,1000);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(OK.value()));

            AddressBooksResponse addressBooksResponse = objectMapper.readValue(result.getResponse().getContentAsString(), AddressBooksResponse.class);
            assertThat(addressBooksResponse, is(notNullValue()));
            assertThat(addressBooksResponse.getData(), is(notNullValue()));
            assertThat(addressBooksResponse.getPaging(), is(notNullValue()));
            assertThat(addressBooksResponse.getData(), is(not(empty())));

            Optional<AddressBook> createdAddressBook = addressBooksResponse.getData().stream().filter(ab -> ab.getId() == createdResponse.getId()).findAny();
            assertTrue(createdAddressBook.isPresent());

            assertThat(createdAddressBook.get().getName(), is(addressBook.getName()));
            assertThat(createdAddressBook.get().getDescription(), is(addressBook.getDescription()));
            assertThat(createdAddressBook.get().getTags(), containsInAnyOrder(addressBook.getTags().toArray()));
        }
        catch (Exception e) {
            fail("Error occurred while testing address book deletion", e);
        }
    }

    @Test
    void testAddressBookDeletion() {
        try {
            AddressBook addressBook = prepareAddressBook();

            MvcResult result = createAddressBook(addressBook);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(CREATED.value()));

            Created createdResponse = objectMapper.readValue(result.getResponse().getContentAsString(), Created.class);

            this.mockMvc
                .perform(
                    delete(BASE_PATH + ADDRESSBOOK_UPDATE_DELETE, GLOBAL_BRANCH_ID, createdResponse.getId())
                        .header(HEADER_CORRELATION_ID, uuid())
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(NO_CONTENT.value()))
                .andReturn();

            result = getAddressBooks(1,1000);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(OK.value()));

            AddressBooksResponse addressBooksResponse = objectMapper.readValue(result.getResponse().getContentAsString(), AddressBooksResponse.class);
            assertThat(addressBooksResponse, is(notNullValue()));
            assertThat(addressBooksResponse.getData(), is(notNullValue()));
            assertThat(addressBooksResponse.getPaging(), is(notNullValue()));
            assertThat(addressBooksResponse.getData(), is(not(empty())));

            Optional<AddressBook> createdAddressBook = addressBooksResponse.getData().stream().filter(ab -> ab.getId() == createdResponse.getId()).findAny();
            assertTrue(createdAddressBook.isEmpty());
        }
        catch (Exception e) {
            fail("Error occurred while testing address book deletion", e);
        }
    }

    /**
     * Test address book retrieval for page that exists
     */
    @Test
    void testAddressBookPaginationDataExists() {
        try {
            int pageSize = 2;

            MvcResult result = getAddressBooks(1,pageSize);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(OK.value()));

            AddressBooksResponse addressBooksResponse = objectMapper.readValue(result.getResponse().getContentAsString(), AddressBooksResponse.class);
            assertThat(addressBooksResponse, is(notNullValue()));
            assertThat(addressBooksResponse.getData(), is(notNullValue()));
            assertThat(addressBooksResponse.getPaging(), is(notNullValue()));
            assertThat(addressBooksResponse.getData(), is(not(empty())));

            result = getAddressBooks(addressBooksResponse.getPaging().getTotalPages(),pageSize);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(OK.value()));

            addressBooksResponse = objectMapper.readValue(result.getResponse().getContentAsString(), AddressBooksResponse.class);
            assertThat(addressBooksResponse, is(notNullValue()));
            assertThat(addressBooksResponse.getData(), is(notNullValue()));
            assertThat(addressBooksResponse.getPaging(), is(notNullValue()));
            assertThat(addressBooksResponse.getData(), is(not(empty())));
        }
        catch (Exception e) {
            fail("Error occurred while testing address book pagination for data available for page that does exist");
        }
    }

    /**
     * Test address books are not retrieved for page that does not exist
     */
    @Test
    void testAddressBookPaginationNoDataAvailable() {
        try {
            MvcResult result = getAddressBooks(1,25);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(OK.value()));

            AddressBooksResponse addressBooksResponse = objectMapper.readValue(result.getResponse().getContentAsString(), AddressBooksResponse.class);
            assertThat(addressBooksResponse, is(notNullValue()));
            assertThat(addressBooksResponse.getData(), is(notNullValue()));
            assertThat(addressBooksResponse.getPaging(), is(notNullValue()));
            assertThat(addressBooksResponse.getData(), is(not(empty())));

            int notAValidPage = addressBooksResponse.getPaging().getTotalPages() + 100;
            result = getAddressBooks(notAValidPage,25);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(OK.value()));

            addressBooksResponse = objectMapper.readValue(result.getResponse().getContentAsString(), AddressBooksResponse.class);
            assertThat(addressBooksResponse, is(notNullValue()));
            assertThat(addressBooksResponse.getData(), is(notNullValue()));
            assertThat(addressBooksResponse.getPaging(), is(notNullValue()));
            assertThat(addressBooksResponse.getData(), is(empty()));
        }
        catch (Exception e) {
            fail("Error occurred while testing address book pagination for data not available for page that does not exist");
        }
    }

    @Test
    void testDuplicateAddressBookCreationFailure() {
        try {
            AddressBook addressBook = prepareAddressBook();

            MvcResult result = createAddressBook(addressBook);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(CREATED.value()));

            result = createAddressBook(addressBook);
            assertThat(result, is(notNullValue()));
            assertThat(result.getResponse().getStatus(), is(BAD_REQUEST.value()));

            assertEquals(
                objectMapper.readValue(result.getResponse().getContentAsString(), ErrorResponse.class)
                    .getErrors().get(0).getCode(),
                ErrorCodes.ERR_DATA_ALREADY_EXISTS
            );
        }
        catch (Exception e) {
            fail("Error occurred while testing failure case when trying to add same address book twice");
        }
    }

    private MvcResult createAddressBook(AddressBook addressBook) throws Exception {
        MvcResult result
            = this.mockMvc
                .perform(
                    post(BASE_PATH + ADDRESSBOOK_GET_CREATE, GLOBAL_BRANCH_ID)
                        .header(HEADER_CORRELATION_ID, uuid())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(addressBook))
                )
            .andReturn();

        return result;
    }

    private MvcResult getAddressBooks(int page, int pageSize) throws Exception {
        MvcResult result
            = this.mockMvc
                .perform(
                    get(BASE_PATH + ADDRESSBOOK_GET_CREATE, GLOBAL_BRANCH_ID)
                        .header(HEADER_CORRELATION_ID, uuid())
                        .param(QUERY_PARAM_PAGE, String.valueOf(page))
                        .param(QUERY_PARAM_PAGELIMIT, String.valueOf(pageSize))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        return result;
    }

    /**
     * Prepare sample address book request
     * @return addressBookRequest
     */
    private AddressBook prepareAddressBook() {
        AddressBook addressBook = new AddressBook();
        addressBook.setName(random(25));
        addressBook.setDescription(random(1000));
        addressBook.setTags(Arrays.asList(random(25), random(25)));

        return addressBook;
    }
}
