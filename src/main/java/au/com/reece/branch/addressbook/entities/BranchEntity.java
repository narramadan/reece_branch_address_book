package au.com.reece.branch.addressbook.entities;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor(staticName = "of")
@Entity
@Table(name = "RCE_BRANCHES")
public class BranchEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @NonNull
    @Column(name = "NAME", unique = true, nullable = false, length = 255)
    private String name;

    @OneToMany(mappedBy = "branch")
    private List<AddressBookEntity> addressBooks = new ArrayList<>();
}