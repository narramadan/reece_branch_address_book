package au.com.reece.branch.addressbook.controller;

import au.com.reece.branch.addressbook.dao.AddressBookRepository;
import au.com.reece.branch.addressbook.dao.BranchRepository;
import au.com.reece.branch.addressbook.dao.ContactRepository;
import au.com.reece.branch.addressbook.entities.AddressBookEntity;
import au.com.reece.branch.addressbook.entities.BranchEntity;
import au.com.reece.branch.addressbook.entities.ContactEntity;
import au.com.reece.branch.addressbook.handler.PrintContactsApi;
import au.com.reece.branch.addressbook.util.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.*;
import java.util.List;
import java.util.UUID;

/**
 * Controller class to export contacts under branch/addressbook to pdf content
 */
@RestController
public class PrintContactsController implements PrintContactsApi {

    @Autowired
    private AddressBookRepository addressBookRepository;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private TemplateEngine templateEngine;

    /**
     * Handle request to print unique contacts under all address books
     *
     * @param xCorrelationId
     * @param branchId
     * @return contacts exported to pdf
     */
    @Override
    public ResponseEntity<Resource> printContactsAcrossAddressBooks(String xCorrelationId, Long branchId) {
        try {
            BranchEntity branch = branchRepository.getById(branchId);
            List<ContactEntity> contactsUnderBranch = contactRepository.findAllContactsUnderBranch(branch, Sort.by("name"));

            // Filter unique contacts across all address books
            List<ContactEntity> distinctContacts = CommonUtils.retrieveDistinctContacts(contactsUnderBranch);

            Resource resource = createContactsReport(branch.getName(), null, distinctContacts);

            String fileName = String.format("Contacts_%s_%s.pdf", replaceSpaces(branch.getName()), UUID.randomUUID().toString());
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Disposition", "inline; filename="+fileName);

            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_PDF)
                    .body(resource);
        }
        catch (Exception e) {
            throw  new RuntimeException(e);
        }
    }

    /**
     * Handle request to print unique contacts under requested address book
     *
     * @param xCorrelationId
     * @param branchId
     * @param addressBookId
     * @return contacts exported to pdf
     */
    @Override
    public ResponseEntity<Resource> printContactsUnderAddressBook(String xCorrelationId, Long branchId, Long addressBookId) {
        try {
            AddressBookEntity addressBook = addressBookRepository.findByIdAndBranchId(addressBookId, branchId).get();
            List<ContactEntity> contacts = contactRepository.findByAddressBook(addressBook, Sort.by("name"));

            Resource resource = createContactsReport(addressBook.getBranch().getName(), addressBook.getName(), contacts);
            String fileName = String.format("%s_%s_%s.pdf", replaceSpaces(addressBook.getBranch().getName()), replaceSpaces(addressBook.getName()), UUID.randomUUID().toString());
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Disposition", "inline; filename="+fileName);

            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_PDF)
                    .body(resource);
        }
        catch (Exception e) {
            throw  new RuntimeException(e);
        }
    }

    /**
     * Method to export contacts into PDF response
     *
     * @param branch
     * @param addressBook
     * @param contacts
     * @return pdf response content
     * @throws Exception
     */
    private Resource createContactsReport(String branch, String addressBook, Iterable<ContactEntity> contacts) throws Exception {

        // Load thymeleaf template and create template engine
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode("HTML");

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        // Put objects in context to use in template
        Context context = new Context();
        context.setVariable("branch", branch);
        context.setVariable("addressbook", addressBook);
        context.setVariable("contacts", contacts);

        String html = templateEngine.process("templates/AddressBookContacts", context);

        // Render html data to pdf content
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(html);
        renderer.layout();
        renderer.createPDF(outputStream);
        outputStream.close();

        InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());

        return new InputStreamResource(inputStream);
    }

    private String replaceSpaces(String text) {
        return text.replaceAll("\\s+", "");
    }
}
