package au.com.reece.branch.addressbook.controller;

import au.com.reece.branch.addressbook.dao.AddressBookRepository;
import au.com.reece.branch.addressbook.dao.ContactRepository;
import au.com.reece.branch.addressbook.entities.*;
import au.com.reece.branch.addressbook.exception.ResourceNotFoundException;
import au.com.reece.branch.addressbook.handler.ContactsApi;
import au.com.reece.branch.addressbook.mapper.ContactMapper;
import au.com.reece.branch.addressbook.model.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;

/**
 * Controller class to manage/retrieve contacts under an address book
 */
@RestController
public class ContactsController implements ContactsApi {

    @Autowired
    private ContactMapper contactMapper;

    @Autowired
    private AddressBookRepository addressBookRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Override
    public ResponseEntity<Created> createAddressBookContact(String xCorrelationId, Long branchId, Long addressBookId, Contact contact) {

        Optional<AddressBookEntity> addressBook = addressBookRepository.findByIdAndBranchId(addressBookId, branchId);
        if(addressBook.isEmpty())
            throw new ResourceNotFoundException(Arrays.asList(Pair.of(ResourceNotFoundException.Resource.branchId, branchId), Pair.of(ResourceNotFoundException.Resource.addressBookId, addressBookId)));

        ContactEntity entity = contactMapper.mapContactToEntity(contact);
        entity.setAddressBook(addressBook.get());

        contactRepository.save(entity);

        Created response = new Created();
        response.setId(entity.getId());

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Void> deleteAddressBookContact(String xCorrelationId, Long branchId, Long addressBookId, Long contactId) {

        Optional<AddressBookEntity> addressBookEntity = addressBookRepository.findByIdAndBranchId(addressBookId, branchId);
        if(addressBookEntity.isEmpty()) throw new ResourceNotFoundException(Arrays.asList(Pair.of(ResourceNotFoundException.Resource.branchId, branchId), Pair.of(ResourceNotFoundException.Resource.addressBookId, addressBookId)));

        Optional<ContactEntity> entity = contactRepository.findByIdAndAddressId(contactId, addressBookEntity.get().getId());
        if(entity.isEmpty()) throw new ResourceNotFoundException(Arrays.asList(Pair.of(ResourceNotFoundException.Resource.addressBookId, branchId), Pair.of(ResourceNotFoundException.Resource.contactId, contactId)));

        contactRepository.delete(entity.get());

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<AddressBookContacts> getAddressBookContacts(String xCorrelationId, Long branchId, Long addressBookId, String contactName, Integer page, Integer limit) {

        Optional<AddressBookEntity> addressBookEntity = addressBookRepository.findByIdAndBranchId(addressBookId, branchId);
        if(isNull(addressBookEntity)) throw new ResourceNotFoundException(Arrays.asList(Pair.of(ResourceNotFoundException.Resource.branchId, branchId), Pair.of(ResourceNotFoundException.Resource.addressBookId, addressBookId)));

        // Retrieve contacts by paginated request and sort by contact name
        Pageable pageable = PageRequest.of(page-1, limit, Sort.by("name"));
        Page<ContactEntity> contactEntities = null;

        // Check if contact name filter is passed in request. If yes, then fetch contacts matching the contact name, else fetch all by oaginated request
        if(StringUtils.isNotEmpty(contactName)) {
            contactEntities = contactRepository.findByAddressBookAndContactName(addressBookEntity.get(), contactName, pageable);
        }
        else {
            contactEntities = contactRepository.findByAddressBook(addressBookEntity.get(), pageable);
        }

        // Map entity object to response model objects
        List<Contact> contacts = contactMapper.mapContactFromEntity(contactEntities.getContent());

        // Set data
        AddressBookContacts response = new AddressBookContacts();
        Contacts data = new Contacts();
        Optional.ofNullable(contacts).ifPresent(data::addAll);
        response.setData(data);

        // Set paging
        Paging paging = new Paging();
        paging.setTotalRecords(contactEntities.getTotalElements());
        paging.setTotalPages(contactEntities.getTotalPages());
        response.setPaging(paging);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> updateAddressBookContact(String xCorrelationId, Long branchId, Long addressBookId, Long contactId, Contact contact) {
        Optional<AddressBookEntity> addressBookEntity = addressBookRepository.findByIdAndBranchId(addressBookId, branchId);
        if(addressBookEntity.isEmpty())
            throw new ResourceNotFoundException(Arrays.asList(Pair.of(ResourceNotFoundException.Resource.branchId, branchId), Pair.of(ResourceNotFoundException.Resource.addressBookId, addressBookId)));

        Optional<ContactEntity> entity = contactRepository.findByIdAndAddressId(contactId, addressBookEntity.get().getId());
        if(entity.isEmpty())
            throw new ResourceNotFoundException(Arrays.asList(Pair.of(ResourceNotFoundException.Resource.addressBookId, branchId), Pair.of(ResourceNotFoundException.Resource.contactId, contactId)));

        contact.setId(entity.get().getId());
        ContactEntity contactEntity = contactMapper.mapContactToEntity(contact, entity.get());

        contactRepository.save(contactEntity);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
