package au.com.reece.branch.addressbook.exception;

import au.com.reece.branch.addressbook.config.ErrorCodesMapper;
import au.com.reece.branch.addressbook.model.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Global class to handle exceptions that are thrown across the application
 */
@ControllerAdvice
public class ExceptionHandlerController {

    Logger log = LoggerFactory.getLogger(ExceptionHandlerController.class);

    @Autowired
    private ErrorCodesMapper errorCodesMapper;

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorResponse onRuntimeException(RuntimeException e) {

        log.error("Error occurred while handling request", e);

        String[] errorDetails = errorCodesMapper.getResponse500ErrorCodes().get(ErrorCodes.ERR_RUNTIME).split("_");

        ErrorResponse error = new ErrorResponse();
        error.setErrors(Arrays.asList(
            new Error(
                ErrorCodes.ERR_RUNTIME,
                errorDetails[0],
                errorDetails[1]
            )
        ));
        return error;
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorResponse onResourceFound(ResourceNotFoundException exception, WebRequest request) {

        String[] errorDetails = errorCodesMapper.getResponse404ErrorCodes().get(ErrorCodes.ERR_RESOURCE_NOT_FOUND).split("_");
        String errorMessage
            = String.format(errorDetails[1], exception.getResources().stream().map(r -> String.format("%s: %s", r.getFirst(), r.getSecond())).collect(Collectors.joining(", ")));

        log.error(String.format("No resource found exception occurred: %s ", errorMessage));

        ErrorResponse response = new ErrorResponse();
        response.setErrors(Arrays.asList(
            new Error(
                ErrorCodes.ERR_RESOURCE_NOT_FOUND,
                errorDetails[0],
                errorMessage)));

        return response;
    }

    @ExceptionHandler(MissingRequestHeaderException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse onInvalidHeader(MissingRequestHeaderException e) {

        String[] errorDetails = errorCodesMapper.getResponse400ErrorCodes().get(ErrorCodes.ERR_HEADER_MISSING).split("_");
        String errorMessage
            = String.format(errorDetails[1], e.getHeaderName());

        log.error(String.format("Invalid request received: %s ", errorMessage), e);

        ErrorResponse response = new ErrorResponse();
        response.setErrors(Arrays.asList(
            new Error(
                ErrorCodes.ERR_HEADER_MISSING,
                errorDetails[0],
                errorMessage)));

        return response;
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse onInvalidRequest(MethodArgumentTypeMismatchException e) {

        String[] errorDetails = errorCodesMapper.getResponse400ErrorCodes().get(ErrorCodes.ERR_REQUEST_PARAM_INVALID).split("_");
        String errorMessage
            = String.format(errorDetails[1], e.getName());

        log.error(String.format("Invalid request received: %s ", errorMessage), e);

        ErrorResponse response = new ErrorResponse();
        response.setErrors(Arrays.asList(
            new Error(
                ErrorCodes.ERR_REQUEST_PARAM_INVALID,
                errorDetails[0],
                errorMessage)));

        return response;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse onInvalidRequest(MethodArgumentNotValidException e) {

        String[] errorDetails = errorCodesMapper.getResponse400ErrorCodes().get(ErrorCodes.ERR_REQUEST_BODY_INVALID).split("_");

        log.error(String.format("Invalid request received"), e);

        FieldError fieldError = e.getFieldError();

        ErrorResponse response = new ErrorResponse();
        response.setErrors(Arrays.asList(
            new Error(
                ErrorCodes.ERR_REQUEST_BODY_INVALID,
                errorDetails[0],
                String.format(errorDetails[1], fieldError.getField(), fieldError.getDefaultMessage()))));

        return response;
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse onDataIntegrityViolationException(DataIntegrityViolationException e) {
        String[] errorDetails = errorCodesMapper.getResponse400ErrorCodes().get(ErrorCodes.ERR_DATA_ALREADY_EXISTS).split("_");

        log.error(String.format("Invalid request received"), e);

        ErrorResponse response = new ErrorResponse();
        response.setErrors(Arrays.asList(
            new Error(
                ErrorCodes.ERR_DATA_ALREADY_EXISTS,
                errorDetails[0],
                errorDetails[1])));

        return response;
    }
}
