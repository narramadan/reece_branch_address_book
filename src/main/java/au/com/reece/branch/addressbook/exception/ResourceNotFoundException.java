package au.com.reece.branch.addressbook.exception;

import lombok.Data;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

/**
 * Custom exception to handle resource that's not available
 */
@Data
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException{

    private static final long serialVersionUID = 1L;
    private List<Pair> resources;

    public static enum Resource {
        branchId,
        addressBookId,
        contactId
    }

    public ResourceNotFoundException(List<Pair> resources) {
        this.resources = resources;
    }

    public ResourceNotFoundException(String message){
        super(message);
    }
}
