package au.com.reece.branch.addressbook.controller;

import au.com.reece.branch.addressbook.config.ErrorCodesMapper;
import au.com.reece.branch.addressbook.dao.AddressBookRepository;
import au.com.reece.branch.addressbook.dao.BranchRepository;
import au.com.reece.branch.addressbook.dao.TagRepository;
import au.com.reece.branch.addressbook.entities.AddressBookEntity;
import au.com.reece.branch.addressbook.entities.BranchEntity;
import au.com.reece.branch.addressbook.entities.TagEntity;
import au.com.reece.branch.addressbook.exception.ResourceNotFoundException;
import static au.com.reece.branch.addressbook.exception.ResourceNotFoundException.Resource;

import au.com.reece.branch.addressbook.handler.AddressBooksApi;
import au.com.reece.branch.addressbook.mapper.AddressBookMapper;
import au.com.reece.branch.addressbook.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Controller class to manage/retrieve address book under a branch
 */
@RestController
public class AddressBookController implements AddressBooksApi {

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private AddressBookRepository addressBookRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private AddressBookMapper addressBookMapper;

    @Autowired
    private ErrorCodesMapper errorCodesMapper;

    @Override
    public ResponseEntity<Created> createAddressBook(String xCorrelationId, Long branchId, AddressBook addressBook) {

        Optional<BranchEntity> branch = branchRepository.findById(branchId);

        if(branch.isEmpty())
            throw new ResourceNotFoundException(Arrays.asList(Pair.of(Resource.branchId, branchId)));

        /*
         * Create TagEntity list from list of tag strings.
         *
         * Save them to tags table and add it to the collection object
         */
        List<TagEntity> tags = new ArrayList<>();
        addressBook.getTags().forEach(tag -> {
            tagRepository.findByName(tag).ifPresentOrElse(t -> tags.add(t), () -> {
                TagEntity tagEntity = TagEntity.of(tag);
                tagRepository.save(tagEntity);
                tags.add(tagEntity);
            });
        });

        // Map addressBook model to entity object
        AddressBookEntity entity = addressBookMapper.mapAddressBookToEntity(addressBook);
        entity.setBranch(branch.get());
        entity.setTags(tags);

        addressBookRepository.save(entity);

        Created response = new Created();
        response.setId(entity.getId());

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Void> deleteAddressBook(String xCorrelationId, Long branchId, Long addressBookId) {

        Optional<BranchEntity> branch = branchRepository.findById(branchId);
        if(branch.isEmpty())
            throw new ResourceNotFoundException(Arrays.asList(Pair.of(Resource.branchId, branchId)));

        Optional<AddressBookEntity> entity = addressBookRepository.findByIdAndBranchId(addressBookId, branch.get().getId());
        if(entity.isEmpty())
            throw new ResourceNotFoundException(Arrays.asList(Pair.of(Resource.branchId, branchId), Pair.of(Resource.addressBookId, addressBookId)));

        addressBookRepository.delete(entity.get());

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<AddressBooksResponse> getAddressBooks(String xCorrelationId, Long branchId, Integer page, Integer limit) {

        Optional<BranchEntity> branch = branchRepository.findById(branchId);
        if(branch.isEmpty())
            throw new ResourceNotFoundException(Arrays.asList(Pair.of(Resource.branchId, branchId)));

        // Retrieve address books by paginated request
        Pageable pageable = PageRequest.of(page-1, limit);
        Page<AddressBookEntity> addressBookEntities = addressBookRepository.findByBranch(branch.get(), pageable);

        // Map entity object to response model objects
        List<AddressBook> addressBooks = addressBookMapper.mapAddressBooksFromEntity(addressBookEntities.getContent());

        AddressBooksResponse response = new AddressBooksResponse();

        // Set data
        AddressBooks data = new AddressBooks();
        Optional.ofNullable(addressBooks).ifPresent(data::addAll);
        response.setData(data);

        // Set paging
        Paging paging = new Paging();
        paging.setTotalRecords(addressBookEntities.getTotalElements());
        paging.setTotalPages(addressBookEntities.getTotalPages());
        response.setPaging(paging);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> updateAddressBook(String xCorrelationId, Long branchId, Long addressBookId, AddressBook addressBook) {

        Optional<BranchEntity> branch = branchRepository.findById(branchId);
        if(branch.isEmpty())
            throw new ResourceNotFoundException(Arrays.asList(Pair.of(Resource.branchId, branchId)));

        Optional<AddressBookEntity> entity = addressBookRepository.findByIdAndBranchId(addressBookId, branch.get().getId());
        if(entity.isEmpty())
            throw new ResourceNotFoundException(Arrays.asList(Pair.of(Resource.branchId, branchId), Pair.of(Resource.addressBookId, addressBookId)));

        /*
         * Create TagEntity list from list of tag strings.
         *
         * Check if new tags are added or tag already exist and then add it to the collection object
         */
        List<TagEntity> tags = new ArrayList<>();
        addressBook.getTags().forEach(tag -> {
            tagRepository.findByName(tag).ifPresentOrElse(t -> tags.add(t), () -> {
                TagEntity tagEntity = TagEntity.of(tag);
                tagRepository.save(tagEntity);
                tags.add(tagEntity);
            });
        });

        AddressBookEntity addressBookEntity = addressBookMapper.mapAddressBookToEntity(addressBook, entity.get());
        addressBookEntity.setBranch(branch.get());
        addressBookEntity.setTags(tags);

        addressBookRepository.save(addressBookEntity);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
