package au.com.reece.branch.addressbook.dao;

import au.com.reece.branch.addressbook.entities.AddressBookEntity;
import au.com.reece.branch.addressbook.entities.BranchEntity;
import au.com.reece.branch.addressbook.entities.ContactEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContactRepository extends PagingAndSortingRepository<ContactEntity, Long> {

    @Query(value = "select contact from ContactEntity contact where contact.id = :id and contact.addressBook.id = :addressBookId")
    Optional<ContactEntity> findByIdAndAddressId(Long id, Long addressBookId);

    List<ContactEntity> findByAddressBook(AddressBookEntity addressBook, Sort sortBy);

    Page<ContactEntity> findByAddressBook(AddressBookEntity addressBook, Pageable pageable);

    @Query(value="FROM ContactEntity c where c.addressBook = :addressBook and lower(c.name) like lower(concat('%', :name, '%')) order by c.name")
    Page<ContactEntity> findByAddressBookAndContactName(AddressBookEntity addressBook, String name, Pageable pageable);

    @Query(value="FROM ContactEntity c inner join fetch c.addressBook a inner join fetch a.branch b where b = :branch order by c.name")
    List<ContactEntity> findAllContactsUnderBranch(BranchEntity branch, Sort sortBy);
}
