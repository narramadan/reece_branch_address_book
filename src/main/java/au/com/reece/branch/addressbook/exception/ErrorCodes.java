package au.com.reece.branch.addressbook.exception;

/**
 * Constant class to hold all error codes thrown from the API
 */
public class ErrorCodes {

    public static final int ERR_HEADER_MISSING = 1001;
    public static final int ERR_REQUEST_PARAM_INVALID = 1002;
    public static final int ERR_REQUEST_BODY_INVALID = 1003;
    public static final int ERR_DATA_ALREADY_EXISTS = 1004;

    public static final int ERR_RESOURCE_NOT_FOUND = 1101;
    public static final int ERR_RUNTIME = 10001;
}
