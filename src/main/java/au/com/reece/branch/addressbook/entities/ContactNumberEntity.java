package au.com.reece.branch.addressbook.entities;

import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor(staticName = "of")
@Entity
@Table(name = "RCE_ADDRESSBOOKS_CONTACT_NUMBERS")
public class ContactNumberEntity {

    public enum ContactNumberType {
        Mobile,Home,Work, Company, Other
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @NonNull
    @Column(name = "TYPE", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private ContactNumberType contactNumberType;

    @NonNull
    @Column(name = "NUMBER", nullable = false, length = 255)
    private String number;

    @Column(name = "EXTENSION", nullable = true, length = 255)
    private String extension;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="CONTACT_ID")
    private ContactEntity contact;

    public static ContactNumberEntity of(ContactNumberType contactNumberType, String number, String extension){
        ContactNumberEntity contactNumber = new ContactNumberEntity();

        contactNumber.contactNumberType = contactNumberType;
        contactNumber.number = number;
        contactNumber.extension = extension;

        return contactNumber;
    }

    @Generated
    @Override
    public String toString() {
        return "ContactNumberEntity{" +
            "id=" + id +
            ", contactNumberType=" + contactNumberType +
            ", number='" + number + '\'' +
            ", extension='" + extension + '\'' +
            '}';
    }
}