package au.com.reece.branch.addressbook.mapper;

import au.com.reece.branch.addressbook.entities.ContactEntity;
import au.com.reece.branch.addressbook.entities.ContactNumberEntity;
import au.com.reece.branch.addressbook.model.Contact;
import au.com.reece.branch.addressbook.model.ContactNumber;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import static java.util.Objects.isNull;
import java.util.stream.Collectors;

/**
 * Mapper class to map Contact entity objects to response model and vice versa
 */
@Component
public class ContactMapper {

    public List<Contact> mapContactFromEntity(List<ContactEntity> contactEntities) {
        List<Contact> contacts = null;
        if(!CollectionUtils.isEmpty(contactEntities)) {
            contacts
                = contactEntities.stream().map(contactEntity -> {

                    Contact contact = new Contact();
                    contact.setId(contactEntity.getId());
                    contact.setName(contactEntity.getName());
                    contact.setNotes(contactEntity.getNotes());
                    contact.setContactNumbers(mapContactNumberFromEntity(contactEntity.getContactNumbers()));

                    return contact;
            }).collect(Collectors.toList());
        }
        return contacts;
    }

    public ContactEntity mapContactToEntity(Contact contact) {
        ContactEntity entity = null;
        if(!isNull(contact)) {
            entity = new ContactEntity();
            entity.setId(contact.getId());
            entity.setName(contact.getName());
            entity.setNotes(contact.getNotes());

            mapContactNumberToEntity(entity, contact.getContactNumbers());
        }
        return entity;
    }

    public ContactEntity mapContactToEntity(Contact contact, ContactEntity entity) {
        if(!isNull(contact) && !isNull(entity)) {
            entity.setId(contact.getId());
            entity.setName(contact.getName());
            entity.setNotes(contact.getNotes());

            mapContactNumberToEntity(entity, contact.getContactNumbers());
        }
        return entity;
    }

    private List<ContactNumber> mapContactNumberFromEntity(List<ContactNumberEntity> contactNumberEntities) {
        List<ContactNumber> contactNumbers = null;
        if(!CollectionUtils.isEmpty(contactNumberEntities)) {
            contactNumbers = contactNumberEntities.stream().map(contactNumberEntity -> {
                ContactNumber contactNumber = new ContactNumber();
                contactNumber.setId(contactNumberEntity.getId());
                contactNumber.setNumber(contactNumberEntity.getNumber());
                contactNumber.setExtension(contactNumberEntity.getExtension());
                contactNumber.setType(ContactNumber.TypeEnum.fromValue(contactNumberEntity.getContactNumberType().toString()));

                return contactNumber;
            }).collect(Collectors.toList());
        }

        return contactNumbers;
    }

    private void mapContactNumberToEntity(ContactEntity entity, List<ContactNumber> contactNumbers) {
        // handle contact numbers that are removed as part of update
        if(!CollectionUtils.isEmpty(entity.getContactNumbers())) {
            List<ContactNumberEntity> removedContactNumbers
                = entity.getContactNumbers().stream().filter(cne ->
                    !contactNumbers.stream().anyMatch(cn -> cne.getId() == cn.getId())
                ).collect(Collectors.toList());
            entity.getContactNumbers().removeAll(removedContactNumbers);

            // update contact numbers which are updated
            entity.getContactNumbers()
                .stream().filter(cne ->
                    contactNumbers.stream().anyMatch(cn -> cne.getId() == cn.getId())
                )
                .forEach(cne -> {
                    ContactNumber contactNumber = contactNumbers.stream().filter(cn -> cn.getId() == cne.getId()).findFirst().get();

                    cne.setNumber(contactNumber.getNumber());
                    cne.setExtension(contactNumber.getExtension());
                    cne.setContactNumberType(ContactNumberEntity.ContactNumberType.valueOf(contactNumber.getType().toString()));
                });
        }

        // Handle new contact numbers
        contactNumbers.stream().filter(cn -> cn.getId() == null).forEach(cn -> {
            ContactNumberEntity contactNumberEntity = new ContactNumberEntity();
            contactNumberEntity.setId(cn.getId());
            contactNumberEntity.setNumber(cn.getNumber());
            contactNumberEntity.setExtension(cn.getExtension());
            contactNumberEntity.setContactNumberType(ContactNumberEntity.ContactNumberType.valueOf(cn.getType().toString()));

            contactNumberEntity.setContact(entity);
            entity.getContactNumbers().add(contactNumberEntity);
        });
    }

}
