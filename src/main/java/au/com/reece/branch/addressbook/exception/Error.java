package au.com.reece.branch.addressbook.exception;

/**
 * Extended swagger generated Error class add constructor to easy object creation
 */
public class Error extends au.com.reece.branch.addressbook.model.Error {

    public Error(Integer code,String message, String details) {
        super();

        this.code(code);
        this.message(message);
        this.details(details);
    }
}
