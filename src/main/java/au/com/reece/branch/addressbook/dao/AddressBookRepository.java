package au.com.reece.branch.addressbook.dao;

import au.com.reece.branch.addressbook.entities.AddressBookEntity;
import au.com.reece.branch.addressbook.entities.BranchEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AddressBookRepository extends PagingAndSortingRepository<AddressBookEntity, Long> {

    Page<AddressBookEntity> findByBranch(BranchEntity branch, Pageable pageable);

    @Query(value = "select address from AddressBookEntity address where address.id = :id and address.branch.id = :branchId")
    Optional<AddressBookEntity> findByIdAndBranchId(Long id, Long branchId);
}
