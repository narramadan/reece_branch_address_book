package au.com.reece.branch.addressbook.entities;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor(staticName = "of")
@Entity
@Table(name = "RCE_ADDRESSBOOKS_CONTACTS")
public class ContactEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @NonNull
    @Column(name = "NAME", unique = true, nullable = false, length = 255)
    private String name;

    @Column(name = "NOTES", nullable = true, length = 1000)
    private String notes;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ADDRESSBOOK_ID")
    private AddressBookEntity addressBook;

    @NonNull
    @OneToMany(mappedBy = "contact", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ContactNumberEntity> contactNumbers = new ArrayList<>();

    public static ContactEntity of(String name, List<ContactNumberEntity> contactNumbers){
        ContactEntity contact = new ContactEntity();
        contact.setName(name);
        contact.setContactNumbers(contactNumbers);

        return contact;
    }

    @Generated
    @Override
    public String toString() {
        return "ContactEntity{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", notes='" + notes + '\'' +
            ", contactNumbers=" + contactNumbers +
            '}';
    }
}
