package au.com.reece.branch.addressbook.config;

import au.com.reece.branch.addressbook.util.YamlPropertySourceFactory;
import lombok.Data;
import lombok.NonNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.Map;

/**
 * Configuration class to load the error codes grouped by the response codes
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "error-codes-mapping")
@PropertySource(value = "classpath:error_codes.yaml", factory = YamlPropertySourceFactory.class)
public class ErrorCodesMapper {

    @NonNull private Map<Integer, String> response400ErrorCodes;
    @NonNull private Map<Integer, String> response404ErrorCodes;
    @NonNull private Map<Integer, String> response500ErrorCodes;
}
