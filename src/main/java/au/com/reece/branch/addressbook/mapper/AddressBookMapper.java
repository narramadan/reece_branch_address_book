package au.com.reece.branch.addressbook.mapper;

import au.com.reece.branch.addressbook.entities.AddressBookEntity;
import au.com.reece.branch.addressbook.model.AddressBook;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import static java.util.Objects.isNull;
import java.util.stream.Collectors;

/**
 * Mapper class to map Address Book entity objects to response model and vice versa
 */
@Component
public class AddressBookMapper {

    public List<AddressBook> mapAddressBooksFromEntity(List<AddressBookEntity> addressBookEntities) {
        List<AddressBook> addressBooks = null;
        if(!CollectionUtils.isEmpty(addressBookEntities)) {
            addressBooks
                    = addressBookEntities.stream().map(addressBookEntity -> {
                AddressBook addressBook = new AddressBook();
                addressBook.setId(addressBookEntity.getId());
                addressBook.setName(addressBookEntity.getName());
                addressBook.setDescription(addressBookEntity.getDescription());

                List<String> tags = addressBookEntity.getTags().stream().map(tagEntity -> tagEntity.getName()).collect(Collectors.toList());
                addressBook.setTags(tags);

                return addressBook;
            }).collect(Collectors.toList());
        }
        return addressBooks;
    }

    public AddressBookEntity mapAddressBookToEntity(AddressBook addressBook) {
        AddressBookEntity entity = null;
        if(!isNull(addressBook)) {
            entity = new AddressBookEntity();
            entity.setId(addressBook.getId());
            entity.setName(addressBook.getName());
            entity.setDescription(addressBook.getDescription());
        }
        return entity;
    }

    public AddressBookEntity mapAddressBookToEntity(AddressBook addressBook, AddressBookEntity entity) {
        if(!isNull(addressBook) && !isNull(entity)) {
            entity.setName(addressBook.getName());
            entity.setDescription(addressBook.getDescription());
        }
        return entity;
    }

}
