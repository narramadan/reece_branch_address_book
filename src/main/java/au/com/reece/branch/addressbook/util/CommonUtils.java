package au.com.reece.branch.addressbook.util;

import au.com.reece.branch.addressbook.entities.ContactEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CommonUtils {

    private static Logger log = LoggerFactory.getLogger(CommonUtils.class);

    /**
     * Uniqueness of contacts across address books is done by name alone. If contact with same name exists in more than one address book.
     *
     * TODO: Need to extend if we can consider contact number entity as well
     *
     * @param contactEntities
     * @return distinct contacts
     */
    public static List<ContactEntity> retrieveDistinctContacts(final List<ContactEntity> contactEntities){

        List<ContactEntity> distinct
            = contactEntities.stream()
                .filter(distinctByKey(c -> c.getName()))
                .sorted(Comparator.comparing(ContactEntity::getName))
                .collect(Collectors.toList());

        return distinct;
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

}