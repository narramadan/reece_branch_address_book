package au.com.reece.branch.addressbook.entities;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor(staticName = "of")
@Entity
@Table(name = "RCE_ADDRESSBOOKS")
public class AddressBookEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @NonNull
    @Column(name = "NAME", unique = true, nullable = false, length = 255)
    private String name;

    @NonNull
    @Column(name = "DESCRIPTION", nullable = true, length = 1000)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="BRANCH_ID")
    private BranchEntity branch;

    @ManyToMany
    @JoinTable(
        name="RCE_ADDRESSBOOKS_TAGS",
            joinColumns = @JoinColumn(name = "ADDRESSBOOK_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "TAG_ID", referencedColumnName = "ID")
    )
    private List<TagEntity> tags = new ArrayList<>();

    @OneToMany(mappedBy = "addressBook", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ContactEntity> contacts = new ArrayList<>();
}