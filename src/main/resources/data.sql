-- Create Branches
INSERT INTO RCE_BRANCHES(NAME) VALUES ('BRCH_ARMADLE');
INSERT INTO RCE_BRANCHES(NAME) VALUES ('BRCH_STKILDA');
INSERT INTO RCE_BRANCHES(NAME) VALUES ('BRCH_RICHMOND_FOR_UNITTEST_USE');

-- Create some tags
INSERT INTO RCE_TAGS(NAME) VALUES ('Plumber');
INSERT INTO RCE_TAGS(NAME) VALUES ('Tradie');
INSERT INTO RCE_TAGS(NAME) VALUES ('Electrician');
INSERT INTO RCE_TAGS(NAME) VALUES ('Emergency');

-- Create few address books for each branch
INSERT INTO RCE_ADDRESSBOOKS(BRANCH_ID, NAME, DESCRIPTION) VALUES (1, 'Emergency Plumbers', 'Contact details of emergency plumbers');
INSERT INTO RCE_ADDRESSBOOKS(BRANCH_ID, NAME, DESCRIPTION) VALUES (1, 'Electricians Near me', 'Contact details of Electricians near to me');
INSERT INTO RCE_ADDRESSBOOKS(BRANCH_ID, NAME, DESCRIPTION) VALUES (1, 'Plumbing Tradies', 'Contact details of plumbing tradies worked previously');

INSERT INTO RCE_ADDRESSBOOKS(BRANCH_ID, NAME, DESCRIPTION) VALUES (2, 'Electricians Plumbers', 'Contact details of emergency electricians');
INSERT INTO RCE_ADDRESSBOOKS(BRANCH_ID, NAME, DESCRIPTION) VALUES (2, 'Plumbers Near me', 'Contact details of Plumbers near to me');
INSERT INTO RCE_ADDRESSBOOKS(BRANCH_ID, NAME, DESCRIPTION) VALUES (2, 'Electricians Tradies', 'Contact details of electrical tradies worked previously');

-- Tag address books with one or more tags
INSERT INTO RCE_ADDRESSBOOKS_TAGS(ADDRESSBOOK_ID, TAG_ID) VALUES (1, 1);
INSERT INTO RCE_ADDRESSBOOKS_TAGS(ADDRESSBOOK_ID, TAG_ID) VALUES (1, 4);
INSERT INTO RCE_ADDRESSBOOKS_TAGS(ADDRESSBOOK_ID, TAG_ID) VALUES (2, 3);
INSERT INTO RCE_ADDRESSBOOKS_TAGS(ADDRESSBOOK_ID, TAG_ID) VALUES (3, 1);
INSERT INTO RCE_ADDRESSBOOKS_TAGS(ADDRESSBOOK_ID, TAG_ID) VALUES (3, 2);

INSERT INTO RCE_ADDRESSBOOKS_TAGS(ADDRESSBOOK_ID, TAG_ID) VALUES (4, 3);
INSERT INTO RCE_ADDRESSBOOKS_TAGS(ADDRESSBOOK_ID, TAG_ID) VALUES (4, 4);
INSERT INTO RCE_ADDRESSBOOKS_TAGS(ADDRESSBOOK_ID, TAG_ID) VALUES (5, 1);
INSERT INTO RCE_ADDRESSBOOKS_TAGS(ADDRESSBOOK_ID, TAG_ID) VALUES (6, 3);
INSERT INTO RCE_ADDRESSBOOKS_TAGS(ADDRESSBOOK_ID, TAG_ID) VALUES (6, 2);

-- Insert few contacts for each address book across different branchs
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(1, 'A Duplicate Contact 1','non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit enim consequat purus. Maecenas libero est, congue a, aliquet vel');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(1, 'A Duplicate Contact 2','et risus. Quisque libero lacus, varius et, euismod et, commodo at, libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in consectetuer ipsum');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(1, 'Zephania Galloway','porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy ac, feugiat non, lobortis quis, pede. Suspendisse dui. Fusce diam nunc, ullamcorper');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(1, 'A Duplicate Contact 3','hendrerit a, arcu. Sed et libero. Proin mi. Aliquam gravida mauris ut mi. Duis risus odio, auctor vitae, aliquet nec, imperdiet nec, leo. Morbi neque tellus, imperdiet non, vestibulum nec');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(1, 'Clio Salinas','Donec dignissim magna a tortor. Nunc commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper, velit in aliquet lobortis, nisi nibh');

INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(2, 'A Duplicate Contact 1','urna. Nullam lobortis quam a felis ullamcorper viverra. Maecenas iaculis aliquet diam. Sed diam lorem, auctor quis, tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(2, 'A Duplicate Contact 2','vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(2, 'Sebastian Fernandez','consequat purus. Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(2, 'Gareth Sheppard','consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum accumsan neque et nunc. Quisque ornare tortor at risus. Nunc ac sem ut dolor dapibus gravida. Aliquam tincidunt, nunc ac mattis ornare');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(2, 'Kato Patel','quis, pede. Suspendisse dui. Fusce diam nunc, ullamcorper eu, euismod ac, fermentum vel, mauris. Integer sem elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed et libero. Proin mi. Aliquam');

INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(3, 'A Duplicate Contact 3','libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(3, 'Rae Carter','Duis dignissim tempor arcu. Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(3, 'Graham Bruce','Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(3, 'Carol Foley','Proin mi. Aliquam gravida mauris ut mi. Duis risus odio, auctor vitae, aliquet nec, imperdiet nec, leo. Morbi neque tellus, imperdiet non, vestibulum nec, euismod in, dolor. Fusce feugiat. Lorem');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(3, 'Branden Owen','Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat, augue');

INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(4, 'Colleen Spencer','mauris ut mi. Duis risus odio, auctor vitae, aliquet nec, imperdiet nec, leo. Morbi neque tellus, imperdiet non, vestibulum nec, euismod in, dolor. Fusce feugiat. Lorem ipsum dolor sit amet');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(4, 'Althea Holden','convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(4, 'Wallace Russell','non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt dui augue eu tellus. Phasellus elit pede, malesuada vel, venenatis vel, faucibus');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(4, 'Tanner Manning','enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed turpis nec mauris blandit mattis. Cras eget nisi dictum augue');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(4, 'Elaine Martin','Proin vel nisl. Quisque fringilla euismod enim. Etiam gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend');

INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(5, 'Lionel Cash','tempus scelerisque, lorem ipsum sodales purus, in molestie tortor nibh sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare sagittis felis. Donec');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(5, 'Igor Maynard','nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec, diam');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(5, 'Leonard Hahn','vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(5, 'Chadwick Carpenter','Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(5, 'Lara Porter','parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra. Donec tempus, lorem fringilla ornare placerat');

INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(6, 'Basia Winters','fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy ac, feugiat non, lobortis quis, pede. Suspendisse dui. Fusce diam nunc');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(6, 'Channing Hurst','mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at fringilla purus mauris a nunc. In at pede. Cras vulputate velit');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(6, 'Acton Saunders','leo, in lobortis tellus justo sit amet nulla. Donec non justo. Proin non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(6, 'Myles Fitzpatrick','egestas. Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare');
INSERT INTO RCE_ADDRESSBOOKS_CONTACTS(ADDRESSBOOK_ID, NAME, NOTES) VALUES(6, 'Echo Watson','Donec luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem');

-- Contact Numbers for the available contacts
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(1,'Mobile','+61 5631 5219','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(1,'Work','+61 2335 1856','246');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(1,'Company','+61 3960 3883','235');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(2,'Mobile','+61 4576 7629','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(2,'Work','+61 1741 7060','286');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(2,'Company','+61 2979 4952','279');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(3,'Mobile','+61 9388 7975','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(3,'Work','+61 8846 1947','272');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(3,'Company','+61 4302 7653','233');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(4,'Mobile','+61 0975 3711','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(4,'Work','+61 2244 1346','244');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(4,'Company','+61 7318 3743','297');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(5,'Mobile','+61 3944 8890','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(5,'Work','+61 2147 8166','275');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(5,'Company','+61 8931 4336','287');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(6,'Mobile','+61 5631 5219','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(6,'Work','+61 2335 1856','246');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(6,'Company','+61 3960 3883','235');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(7,'Mobile','+61 4576 7629','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(7,'Work','+61 1741 7060','286');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(7,'Company','+61 2979 4952','279');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(8,'Company','+61 1743 6236','271');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(8,'Mobile','+61 1077 2841','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(8,'Work','+61 5829 6604','290');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(9,'Company','+61 4775 2883','260');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(9,'Mobile','+61 4223 1429','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(9,'Work','+61 7209 4533','230');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(10,'Company','+61 1242 9090','252');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(10,'Mobile','+61 9255 8270','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(10,'Work','+61 8941 4132','245');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(11,'Mobile','+61 0975 3711','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(11,'Work','+61 2244 1346','244');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(11,'Company','+61 7318 3743','297');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(12,'Company','+61 2177 5962','294');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(12,'Mobile','+61 7575 5753','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(12,'Work','+61 1086 7035','264');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(13,'Company','+61 0611 4934','259');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(13,'Mobile','+61 1212 8870','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(13,'Mobile','+61 7440 4773','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(14,'Work','+61 5916 5391','276');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(14,'Company','+61 8018 7900','234');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(14,'Mobile','+61 5964 9075','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(15,'Work','+61 8555 0840','202');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(15,'Company','+61 8134 4219','270');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(15,'Mobile','+61 5883 4514','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(16,'Work','+61 9702 5662','270');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(16,'Company','+61 2797 1063','263');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(16,'Mobile','+61 9773 2215','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(17,'Work','+61 7094 2331','221');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(17,'Company','+61 5562 1575','247');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(17,'Mobile','+61 5569 9532','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(18,'Work','+61 9615 7503','228');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(18,'Company','+61 6859 3991','265');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(18,'Mobile','+61 3590 1508','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(19,'Work','+61 3605 0884','270');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(19,'Company','+61 7542 1980','215');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(19,'Mobile','+61 8083 5365','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(20,'Mobile','+61 4517 2615','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(20,'Work','+61 6334 1916','241');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(20,'Company','+61 7871 8594','274');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(21,'Mobile','+61 8125 7195','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(21,'Work','+61 0164 0199','214');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(21,'Company','+61 4123 7782','249');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(22,'Mobile','+61 9041 6778','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(22,'Work','+61 0333 7126','285');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(22,'Company','+61 9570 4177','228');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(23,'Mobile','+61 6113 5916','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(23,'Work','+61 6655 8397','215');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(23,'Company','+61 1147 1608','272');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(24,'Mobile','+61 8417 4507','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(24,'Work','+61 3188 5512','207');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(24,'Company','+61 0408 3906','240');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(25,'Mobile','+61 8066 7346','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(25,'Work','+61 4196 4548','265');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(25,'Company','+61 2880 3997','207');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(27,'Mobile','+61 9586 5463','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(27,'Mobile','+61 3458 6295','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(27,'Work','+61 2805 4442','239');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(28,'Company','+61 0541 3396','265');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(28,'Mobile','+61 3978 0999','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(28,'Work','+61 5138 9978','285');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(29,'Work','+61 1110 5794','239');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(29,'Company','+61 8466 2829','248');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(29,'Mobile','+61 8856 9737','');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(30,'Work','+61 4043 4980','211');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(30,'Work','+61 6193 7797','257');
INSERT INTO RCE_ADDRESSBOOKS_CONTACT_NUMBERS(CONTACT_ID, TYPE, NUMBER, EXTENSION) VALUES(30,'Company','+61 7461 1465','204');