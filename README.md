# Branch Manager Address Book API

## Introduction

Branch Manager Address Book API is implemented to cater the business requirement as below:

    As a Reece Branch Manager
    I would like an address book application
    So that I can keep track of my customer contacts

Refer to API specs documented in [`src/main/resources/static/swagger.yaml`](https://editor.swagger.io/?url=https://bitbucket.org/narramadan/reece_branch_address_book/raw/e9610760360b6a9bd223326485e711c645cfc43d/src/main/resources/static/swagger.yaml) or access `http://localhost:8080/swagger-ui/` if the API is up & running.

__Read/Manage Address Book(s)__

| HTTP Method | API Name | Path | Response Status Code |
| -------- | ----------------- | ---- | -------------------------- |
| POST | Create an Address Book | `/branches/{branchId}/addressbooks` | 201 |
| GET | Get Address Books | `/branches/{branchId}/addressbooks` | 200 |
| PUT | Update an Address Book | `/branches/{branchId}/addressbooks/{addressBookId}` | 200 |
| DELETE | Delete an Address Book | `/branches/{branchId}/addressbooks/{addressBookId}` | 204 |

__Read/Manage Contact(s) under an Address Book__

| HTTP  Method | API Name | Path | Response  Status Code |
| -------- | ----------------- | ---- | -------------------------- |
| POST | Create a Contact | `/branches/{branchId}/addressbooks/{addressBookId}/contacts` | 201 |
| GET | Get Contacts | `/branches/{branchId}/addressbooks/{addressBookId}/contacts` | 200 |
| PUT | Update a Contact | `/branches/{branchId}/addressbooks/{addressBookId}/contacts/{contactId}` | 200 |
| DELETE | Delete a Contact | `/branches/{branchId}/addressbooks/{addressBookId}/contacts/{contactId}` | 204 |

__Print Contact(s)__

| HTTP  Method | API Name | Path | Response  Status Code |
| -------- | ----------------- | ---- | -------------------------- |
| GET | Print contacts under address book | `/branches/{branchId}/addressbooks/{addressBookId}/print` | 200 |
| GET | Print unique contacts across all address books | `/branches/{branchId}/addressbooks/print` | 200 |

## Technology stack used for implementing this API

* OpenJDK 11
* Spring Boot 2.5.4
* Spring Data JPA with Hibernate
* H2 In-memory database
* Thymeleaf Template Engine 
* Flying Saucer PDF
* Spring Test with jUnit 5 & Hamcrest
* Apache Maven
* JaCoCo for Code Coverage
* Postman for API Integration Testing

## Considerations
* API Spec & DB Schema are designed with consideration that address books are maintained at branch level.
    * Branch is key element to address books and is needed to manage/retrieve address books.
    * No endpoint available to retrieve/manage branch(s) in this API
* In-memory H2 Database is used in this API. DB Schema creation and seed data is loaded during startup using `src/resources/schema.sql` && `src/resources/data.sql`.
    * 3 branches are created of which brancId=1 is used in unit/integration tests and branchId=3 is used for print controller unit tests
    * Couple of address books and contacts are created for testing

## Limitations
* API doesn't include any authentication.
* DB Schema doesn't include Audit information to track when/by whom data insert,update is performed.
* Uniqueness of contacts across address books is done by name alone.
* Spring profiles are not used in this version and always uses in-memory H2 db as datasource.
* Ensure to run single instance of API in kubernetes, else it's going to be a chaos with each instance having its own database.

## Configure & Package the API

Follow the below steps to build and execute Branch Manager Address Book API on your machine.

* Ensure you have `Java 11` installed and configured before proceeding further. If not available, follow the below steps to set up Java 11
    * Download [Zulu OpenJDK 11 Win x64 MSI](https://cdn.azul.com/zulu/bin/zulu11.50.19-ca-jdk11.0.12-win_x64.msi) or choose [appropriate](https://www.azul.com/downloads/zulu-community/?version=java-11-lts&package=jdk) based on your machine.
    * Proceed to execute the installer by choosing the appropriate location or stick to default.
    * Configure `PATH` & `JAVA_HOME` system environment variable to point to the installation location. Follow [Instructions](https://javatutorial.net/set-java-home-windows-10) for Windows 10.
    * Run `java -version` in your command prompt to confirm Java is configured and working.

* Clone the project from [Bitbucket Repo](https://bitbucket.org/narramadan/reece_branch_address_book/src/master/)
```
$ git clone https://bitbucket.org/narramadan/reece_branch_address_book.git
```

* Run below `maven` commands in command prompt from the root of the application. This should compile, execute tests and build `Executable JAR`.
    * Initial build will take couple of minutes to download packages and dependencies required to build the application. Subsequent builds will be faster.

```
D:\Work\samples\reece_branch_address_book (develop -> origin)
λ .\mvnw.cmd clean package
[INFO] Scanning for projects...
[INFO]
[INFO] ----------------< au.com.reece:branch-addressbook-api >-----------------
[INFO] Building branch-addressbook-api 0.0.1-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
...
...
...
[INFO] Tests run: 24, Failures: 0, Errors: 0, Skipped: 0
[INFO]
[INFO]
[INFO] --- jacoco-maven-plugin:0.8.5:report (report) @ branch-addressbook-api ---
[INFO] Loading execution data file D:\Work\samples\reece_branch_address_book\target\jacoco.exec
[INFO] Analyzed bundle 'branch-addressbook-api' with 15 classes
[INFO]
[INFO] --- maven-jar-plugin:3.2.0:jar (default-jar) @ branch-addressbook-api ---
[INFO] Building jar: D:\Work\samples\reece_branch_address_book\target\branch-addressbook-api-0.0.1-SNAPSHOT.jar
[INFO]
[INFO] --- spring-boot-maven-plugin:2.5.4:repackage (repackage) @ branch-addressbook-api ---
[INFO] Replacing main artifact with repackaged archive
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  02:41 min
[INFO] Finished at: 2021-09-13T20:24:21+10:00
[INFO] ------------------------------------------------------------------------
```

## Running and Testing the API

* Execute the below `maven` command using `spring-boot` plugin to run the API locally on `8080` port
```
$ mvn spring-boot:run
```

* API swagger spec can be accessed by navigating to `http://localhost:8080/swagger-ui/`

* In-memory H2 database console can be accessed by navigating to `http://localhost:8080/h2`
    * Set jdbc url - `jdbc:h2:mem:branch-addressbook-db`
    * Keep the rest as-is and login to the console
    * Tables created as part of startup can be viewed post login

* Import `./api-postman-tests/dev-collectionI.postman_collection.json` to postman to load the collection which can be used to test the API upon startup.
    * Postman API requests are pre-configured with test data and ready to be invoked to test different actions

## Execute Integration Tests

API Integration Tests are implemented with postman and can be executed by importing `./api-postman-tests/integrationtests.postman_collection.json` to postman or running the below `newman` command.

Verify if NodeJS is installed by executing below command. If not, install NodeJs to proceed with `newman`.

```
$ node -v

# Install newman if not available
$ npm install -g newman

$ newman -v
```

Execute `newman` command from root of the application to run the `Integration Tests` 
```
$ newman run api-postman-tests\integrationtests.postman_collection.json
Branch Manager Address Book API Integration Test

→ 10-Create a new address book
  POST http://localhost:8080/v1/branches/1/addressbooks [201 Created, 177B, 486ms]
  √  Status code is 201
  √  extract addressBookId
...
...
...  
┌─────────────────────────┬────────────────────┬────────────────────┐
│                         │           executed │             failed │
├─────────────────────────┼────────────────────┼────────────────────┤
│              iterations │                  1 │                  0 │
├─────────────────────────┼────────────────────┼────────────────────┤
│                requests │                 10 │                  0 │
├─────────────────────────┼────────────────────┼────────────────────┤
│            test-scripts │                 20 │                  0 │
├─────────────────────────┼────────────────────┼────────────────────┤
│      prerequest-scripts │                 11 │                  0 │
├─────────────────────────┼────────────────────┼────────────────────┤
│              assertions │                 15 │                  0 │
├─────────────────────────┴────────────────────┴────────────────────┤
│ total run duration: 2.7s                                          │
├───────────────────────────────────────────────────────────────────┤
│ total data received: 5.79kB (approx)                              │
├───────────────────────────────────────────────────────────────────┤
│ average response time: 169ms [min: 14ms, max: 810ms, s.d.: 253ms] │
└───────────────────────────────────────────────────────────────────┘
```

## Create Docker image and start the container

Execute the below commands to build `docker` image and push it to registry.

* Build the image using `spring-boot` maven plugin
```
$ mvn clean spring-boot:build-image
```

* Verify if the image is created
```
$ docker images branch-addressbook-api
```

* Test the image by starting the docker container and executing imported postman collection in above step
```
$ docker run -it -p8080:8080 branch-addressbook-api:0.0.1-SNAPSHOT
```

Tag and push the image to registry
```
$ docker image tag branch-addressbook-api:0.0.1-SNAPSHOT <docker-hub-username>/branch-addressbook-api:0.0.1-SNAPSHOT

$ docker push <docker-hub-username>/branch-addressbook-api:0.0.1-SNAPSHOT
```

## Deploy to Kubernetes (Docker Desktop)

Below deployment procedure to kubernetes is done by __considering__ that k8s cluster is __running locally__ via `Docker Desktop` and image is pulled from docker hub with tagged user - `narramadan`.

API service yaml is configured at `k8s\branch-addressbook-api-service.yml` with hardcoded image name with docker hub username.

* Verify if k8s cluster is up & running
```
$ kubectl cluster-info
```

* Orchestrate the deployment
```
$ kubectl apply -f k8s
```

* Verify if service is provisioned and running. Replace pod name in below queries
```
$ kubectl get all 
$ kubectl get pods --watch

$ kubectl exec pod/branch-addressbook-api-<XXX-XXX> -- printenv | grep SERVICE
$ kubectl logs pod/branch-addressbook-api-<XXX-XXX>
```

* Run API Integeration tests from post or run newman from root of the project - `newman run api-postman-tests\integrationtests.postman_collection.json`

* Delete the services
```
$ kubectl delete -f k8s
```

## Improvements Needed

* Need to capture multiple validation errors in single shot. Somehow validation on all params is not happening to capture BindException and retrieve all validation errors.
* Current implementation considers unique contacts across address books is by contact name. Need to extend to identify distinct contacts by name and phone numbers.
* Can you mapstruct to minimize entity<->model object mapping code.